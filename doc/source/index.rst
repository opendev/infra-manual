:title: OpenDev Manual

.. _infra_manual:

OpenDev Manual
##############

The projects hosted by OpenDev use a number of specialized tools and
processes while developing software that is part of the projects. This
manual will help you learn how to use them as a developer working on
any project hosted in the OpenDev infrastructure.

.. important::

   You can always check the status of the OpenDev CI infrastructure via:

   * https://wiki.openstack.org/wiki/Infrastructure_Status
   * `@opendevinfra@fosstodon.org
     <https://fosstodon.org/@opendevinfra>`_ in the fediverse
   * the topic in IRC

   If you notice problems, report them via
   :ref:`#opendev on IRC <irc-technical-support>` or
   use the `service-discuss mailing list
   <http://lists.opendev.org/cgi-bin/mailman/listinfo/service-discuss>`_.

.. toctree::
   :maxdepth: 2

   gettingstarted
   developers
   irc
   core
   drivers
   creators
   sandbox
   testing

Many projects hosted on OpenDev have their own Contributor Guides that
explain project specific information. A non-complete list of these
are:

* `OpenStack Contributors Guide
  <https://docs.openstack.org/contributors>`__
* `StarlingX Contributor Guides
  <https://docs.starlingx.io/contributor/index.html>`__


Propose changes to this document to the `infra-manual git repository
<https://opendev.org/opendev/infra-manual>`_.
